import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  constructor(private httpClient: HttpClient) { }

  private apiServer = 'http://localhost:3000/cards';

  getAll(): Observable<any> {
    return this.httpClient.get<any>(this.apiServer + '/s');
  }

  getById(id): Observable<any> {
    return this.httpClient.get<any>(`${this.apiServer}/${id}`);
  }

  create(model): Observable<any> {
    return this.httpClient.post<any>(`${this.apiServer}`, model);
  }

  update(id, model): Observable<any> {
    return this.httpClient.put<any>(`${this.apiServer}/${id}`, model);
  }

  delete(id): Observable<any> {
    return this.httpClient.delete<any>(`${this.apiServer}/${id}`);
  }

}
