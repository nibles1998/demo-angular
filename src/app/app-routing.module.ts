import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsComponent } from './components/cards/cards.component';
import { CardsUpdateComponent } from './components/cards-update/cards-update.component';
import { CardsCreateComponent } from './components/cards-create/cards-create.component';


const routes: Routes = [
  {
    path: '',
    component: CardsComponent
  },
  {
    path: 'update/:_id',
    component: CardsUpdateComponent
  },
  {
    path: 'create',
    component: CardsCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
