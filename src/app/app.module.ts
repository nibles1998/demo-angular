import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardsComponent } from './components/cards/cards.component';
import { HttpClientModule } from '@angular/common/http';
import { CardsUpdateComponent } from './components/cards-update/cards-update.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardsCreateComponent } from './components/cards-create/cards-create.component';

@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    CardsUpdateComponent,
    CardsCreateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
