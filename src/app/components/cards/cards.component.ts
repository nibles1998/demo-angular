import { Component, OnInit } from '@angular/core';
import { CardsService } from '../../services/cards.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  cards: any[];
  constructor(
    private cardsService: CardsService
  ) { }

  ngOnInit(): void {
    this.cardsService.getAll().subscribe((res) => {
      const data = res.data;
      for (const i of data) {
        const expirationDate = i.expirationDate.split('T')[0];
        i.expirationDate = expirationDate;
      }
      this.cards = data;
      console.log(res);
    });
  }
  delete(id): void {
    this.cardsService.delete(id).subscribe((res) => {
      console.log(res);
      this.cardsService.getAll().subscribe((data) => {
        this.cards = data.data;
        console.log(data);
      });
    });
  }
}
