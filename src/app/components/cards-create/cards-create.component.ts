import { Component, OnInit } from '@angular/core';
import { CardsService } from '../../services/cards.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards-create',
  templateUrl: './cards-create.component.html',
  styleUrls: ['./cards-create.component.scss']
})
export class CardsCreateComponent implements OnInit {

  formGroup: FormGroup;
  constructor(
    private cardsService: CardsService,
    private router: Router,
    private formBuilder: FormBuilder

  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      numberCard: new FormControl(),
      type: new FormControl(),
      expirationDate: new FormControl(),
    });
  }

  create(): any {
    this.cardsService.create(this.formGroup.value).subscribe((res) => {
      this.router.navigateByUrl('/');
    });
  }

}
