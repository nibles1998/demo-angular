import { Component, OnInit } from '@angular/core';
import { CardsService } from '../../services/cards.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cards-update',
  templateUrl: './cards-update.component.html',
  styleUrls: ['./cards-update.component.scss']
})
export class CardsUpdateComponent implements OnInit {

  card: any;
  formGroup: FormGroup;
  id: string;
  constructor(
    private cardsService: CardsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,

  ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params._id;
    this.cardsService.getById(this.id).subscribe((res) => {
      this.getInfo(res.data);
    });
  }
  update(): any {
    this.cardsService.update(this.id, this.formGroup.value).subscribe((res) => {
      this.router.navigateByUrl('/');
    });
  }

  getInfo(card): any {
    this.formGroup = this.formBuilder.group({
      numberCard: card.numberCard,
      type: card.type,
      expirationDate: card.expirationDate.toString().split('T')[0]
    });
  }

}
